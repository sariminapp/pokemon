//
//  Detail+CoreDataProperties.swift
//  
//
//  Created by Oscar Perdanakusuma Adipati on 06/12/20.
//
//

import Foundation
import CoreData


extension Detail {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<Detail> {
        return NSFetchRequest<Detail>(entityName: "Detail")
    }

    @NSManaged public var id: String?
    @NSManaged public var captureRate: Int64
    @NSManaged public var habitat: String?
    @NSManaged public var generation: String?
    @NSManaged public var descriptions: String?
    @NSManaged public var hatchCounter: Int64
    @NSManaged public var spritesNormal: String?
    @NSManaged public var spritesShiny: String?
    @NSManaged public var female: Int64
    @NSManaged public var baseStats: [Int]?
    @NSManaged public var statNames: [String]?
    @NSManaged public var abNames: [String]?
    @NSManaged public var weaknessNames: [String]?
    @NSManaged public var weaknessDouble: [Double]?
    @NSManaged public var eggGroupNames: [String]?
    @NSManaged public var evolutionNames: [String]?
    @NSManaged public var evolutionLevels: [Int]?
    @NSManaged public var evolutionURLs: [String]?
    @NSManaged public var pokemons: Pokemon?

}
