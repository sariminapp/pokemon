//
//  Pokemon+CoreDataProperties.swift
//  
//
//  Created by Oscar Perdanakusuma Adipati on 06/12/20.
//
//

import Foundation
import CoreData


extension Pokemon {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<Pokemon> {
        return NSFetchRequest<Pokemon>(entityName: "Pokemon")
    }

    @NSManaged public var id: String?
    @NSManaged public var name: String?
    @NSManaged public var image: String?
    @NSManaged public var url: String?
    @NSManaged public var type: NSObject?

}
