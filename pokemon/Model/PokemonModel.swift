//
//  PokemonModel.swift
//  pokemon
//
//  Created by Oscar Perdanakusuma Adipati on 03/12/20.
//

import ObjectMapper
import Foundation

class PokemonListModel : NSObject, Mappable{
    
    var count: Int?
    var results: [resultsData]?
    
    required init?(map: Map){}
    
    func mapping(map: Map) {
        results <- map["results"]
        count <- map["count"]
    }
    
}

class resultsData : NSObject, Mappable{
    
    var name, url: String?
    
    required init?(map: Map){}
    
    func mapping(map: Map) {
        name <- map["name"]
        url <- map["url"]
    }
    
}

class PokemonModel : Mappable{
    
    var abilities: [abilitiesData]?
    var stats: [statsData]?
    var types: [typesData]?
    var sprites: spritesData?
    
    required init?(map: Map){}
    
    func mapping(map: Map) {
        abilities <- map["abilities"]
        stats <- map["stats"]
        types <- map["types"]
        sprites <- map["sprites"]
    }
    
}

class spritesData : NSObject, Mappable{
    
    var front_default, front_shiny: String?
    
    required init?(map: Map){}
    
    func mapping(map: Map) {
        front_default <- map["front_default"]
        front_shiny <- map["front_shiny"]
    }
    
}

class abilitiesData : NSObject, Mappable{
    
    var ability: resultsData?
    var is_hidden: Bool?
    var slot: Int?
    
    required init?(map: Map){}
    
    func mapping(map: Map) {
        ability <- map["ability"]
        is_hidden <- map["is_hidden"]
        slot <- map["slot"]
    }
    
}

class statsData : NSObject, Mappable{
    
    var base_stat: Int?
    var stat: resultsData?
    
    required init?(map: Map){}
    
    func mapping(map: Map) {
        base_stat <- map["base_stat"]
        stat <- map["stat"]
    }
    
}

class typesData : NSObject, Mappable{
    
    var slot: Int?
    var typea: resultsData?
    
    required init?(map: Map){}
    
    func mapping(map: Map) {
        slot <- map["slot"]
        typea <- map["type"]
    }
    
}

class PokemonSpeciesModel : NSObject, Mappable{
    
    var capture_rate, hatch_counter, gender_rate: Int?
    var color, generation, habitat, evolution_chain: resultsData?
    var egg_groups: [resultsData]?
    var form_descriptions: [formDescriptionsData]?
    
    required init?(map: Map){}
    
    func mapping(map: Map) {
        color <- map["color"]
        capture_rate <- map["capture_rate"]
        egg_groups <- map["egg_groups"]
        form_descriptions <- map["form_descriptions"]
        generation <- map["generation"]
        habitat <- map["habitat"]
        hatch_counter <- map["hatch_counter"]
        gender_rate <- map["gender_rate"]
        evolution_chain <- map["evolution_chain"]
    }
    
}

class PokemonTypeModel : NSObject, Mappable{
    
    var damage_relations: damageRelations?
    var id: Int?
    var generation, move_damage_class: resultsData?
    var moves: [resultsData]?
    
    required init?(map: Map){}
    
    func mapping(map: Map) {
        damage_relations <- map["damage_relations"]
        generation <- map["generation"]
        id <- map["id"]
        move_damage_class <- map["move_damage_class"]
        moves <- map["moves"]
    }
    
}

class damageRelations : Mappable{
    
    var double_damage_from, half_damage_from, no_damage_from: [resultsData]?
    
    required init?(map: Map){}
    
    func mapping(map: Map) {
        double_damage_from <- map["double_damage_from"]
        half_damage_from <- map["half_damage_from"]
        no_damage_from <- map["no_damage_from"]
    }
    
}

class formDescriptionsData : NSObject, Mappable{
    
    var descriptiona: String?
    var language: resultsData?
    
    required init?(map: Map){}
    
    func mapping(map: Map) {
        descriptiona <- map["description"]
        language <- map["language"]
    }
    
}

class PokemonGenderModel : NSObject, Mappable{
    
    var name: String?
    var pokemon_species_details: [pokeSpecData]?
    
    required init?(map: Map){}
    
    func mapping(map: Map) {
        name <- map["name"]
        pokemon_species_details <- map["pokemon_species_details"]
    }
    
}

class pokeSpecData : NSObject, Mappable{
    
    var rate: Int?
    var pokemon_species: resultsData?
    
    required init?(map: Map){}
    
    func mapping(map: Map) {
        pokemon_species <- map["pokemon_species"]
        rate <- map["rate"]
    }
    
}

class PokemonEvolutionModel : NSObject, Mappable{
    
    var chain: chainData?
    
    required init?(map: Map){}
    
    func mapping(map: Map) {
        chain <- map["chain"]
    }
    
}

class chainData : NSObject, Mappable{
    
    var species: resultsData?
    var evolves_to: [evolvesDatas]?
    
    required init?(map: Map){}
    
    func mapping(map: Map) {
        species <- map["species"]
        evolves_to <- map["evolves_to"]
    }
    
}

class evolvesDatas : NSObject, Mappable{
    
    var species: resultsData?
    var evolves_to: [evolvesDatas]?
    var evolution_details: [evolutionDetailDatas]?
    
    required init?(map: Map){}
    
    func mapping(map: Map) {
        species <- map["species"]
        evolves_to <- map["evolves_to"]
        evolution_details <- map["evolution_details"]
    }
    
}

class evolutionDetailDatas : NSObject, Mappable{
    
    var min_level: Int?
    var trigger: resultsData?
    
    required init?(map: Map){}
    
    func mapping(map: Map) {
        min_level <- map["min_level"]
        trigger <- map["trigger"]
    }
    
}
