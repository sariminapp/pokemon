//
//  LoginRegisterModel.swift
//  homecredit
//
//  Created by Oscar Perdanakusuma on 8/16/17.
//  Copyright © 2017 Home Credit Indonesia. All rights reserved.
//

import ObjectMapper
import Foundation

class LoginRegisModel : MainResponse{
    
    var user_id : String?
    var token : String?
    var role : Int?
    var first_time : Bool?
    var detail : String?
    
    override func mapping(map: Map) {
        user_id <- map["user_id"]
        token <- map["token"]
        role <- map["role"]
        first_time <- map["first_time"]
        detail <- map["detail"]
        super .mapping(map: map)
    }
    
}

class LoginModel : Mappable{
    
    var data: [Datas]?
    var code: String?
    var message: String?
    
    required init?(map: Map){}
    
    func mapping(map: Map) {
        data <- map["data"]
        code <- map["code"]
        message <- map["message"]
    }
    
}

class RegisterModel : Mappable{
    
    var data: [Datas]?
    var code: String?
    var message: String?
    
    required init?(map: Map){}
    
    func mapping(map: Map) {
        data <- map["data"]
        code <- map["code"]
        message <- map["message"]
    }
    
}

class Datas : NSObject, Mappable{
    
    var clientID: String?
    var clientName: String?
    var clientPassword: String?
    var clientEmail: String?
    var clientAddress: String?
    var clientPhone: String?
    
    required init?(map: Map){}
    
    func mapping(map: Map) {
        clientID <- map["client_id"]
        clientName <- map["client_name"]
        clientPassword <- map["client_password"]
        clientEmail <- map["client_email"]
        clientAddress <- map["client_address"]
        clientPhone <- map["client_phone"]
    }
    
}

class ForgotPasswordModel : MainResponse{
    
    var email : String?
    
    override func mapping(map: Map) {
        email <- map["email"]
        super .mapping(map: map)
    }
    
}

class RefreshTokenModel : MainResponse{
    
    var access : String?
    var refresh : String?
    var non_field_errors : [String]?
    var password : [String]?
    var username : [String]?
    
    override func mapping(map: Map) {
        access <- map["access"]
        refresh <- map["refresh"]
        non_field_errors <- map["non_field_errors"]
        password <- map["password"]
        username <- map["username"]
        super .mapping(map: map)
    }
    
}

class ForgotPassword2Model : NSObject, Mappable{
    
    var message: String?
    
    required init?(map: Map){}
    
    func mapping(map: Map) {
        message <- map["message"]
    }
    
}




