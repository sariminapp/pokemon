//
//  CustomCellView.swift
//  pokemon
//
//  Created by Oscar Perdanakusuma Adipati on 03/12/20.
//

import Foundation
import UIKit
import Kingfisher

class mainTableViewCell: UITableViewCell {

    @IBOutlet private weak var collectionView: UICollectionView!
    @IBOutlet weak var lblPokemonName: UILabel!
    @IBOutlet weak var imgPokemon: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
    }
    
    func setCollectionViewDataSourceDelegate(dataSourceDelegate: UICollectionViewDataSource & UICollectionViewDelegate, forRow row: Int) {
        collectionView.delegate = dataSourceDelegate
        collectionView.dataSource = dataSourceDelegate
        collectionView.tag = row
        collectionView.semanticContentAttribute = .forceRightToLeft
        collectionView.reloadData()
    }

}

class PokemonTypeCellView: UICollectionViewCell {
    
    @IBOutlet weak var imgPokemonType: UIImageView!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
}

class PokemonDetailTypeCellView: UICollectionViewCell {
    
    @IBOutlet weak var imgPokemonType: UIImageView!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
}

class statsTableViewCell: UITableViewCell {

    @IBOutlet weak var lblStatsName: UILabel!
    @IBOutlet weak var lblStatsInt: UILabel!
    @IBOutlet weak var progStats: UIProgressView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
    }

}

class PokemonWeaknessTypeCellView: UICollectionViewCell {
    
    @IBOutlet weak var imgWeaknessType: UIImageView!
    @IBOutlet weak var lblWeakStats: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
}

class breedingTableViewCell: UITableViewCell {

    @IBOutlet weak var lblEggGroup: UILabel!
    @IBOutlet weak var lblHatchTime: UILabel!
    @IBOutlet weak var lblGender: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
    }

}

class captureTableViewCell: UITableViewCell {

    @IBOutlet weak var lblHabitat: UILabel!
    @IBOutlet weak var lblGeneration: UILabel!
    @IBOutlet weak var lblCaptureRate: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
    }

}

class evolutionTableViewCell: UITableViewCell {

    @IBOutlet weak var lblName1: UILabel!
    @IBOutlet weak var lblLevel: UILabel!
    @IBOutlet weak var lblName2: UILabel!
    @IBOutlet weak var imgSpecies1: UIImageView!
    @IBOutlet weak var imgSpecies2: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
    }

}
