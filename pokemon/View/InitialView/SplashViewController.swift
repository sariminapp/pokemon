//
//  SplashViewController.swift
//  pokemon
//
//  Created by Oscar Perdanakusuma Adipati on 03/12/20.
//

import UIKit
import CoreData
import Alamofire
import AlamofireObjectMapper

class SplashViewController : UIViewController {
    
    public static var isShowPINLock : Bool!
    let colors = gradientColors()
    
    override func viewDidLoad() {
        super .viewDidLoad()
        SplashViewController.isShowPINLock = false
        AppController.sharedInstance.setClearGlobalObject()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
            self.gotoHomePage()
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
//        CommonUtils.AppUtility.lockOrientation(.portrait)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
//        CommonUtils.AppUtility.lockOrientation(.portrait)
    }
    
    func gotoHomePage(){
        let storyboard = UIStoryboard(name: Constant.sharedIns.MAIN_STORYBOARD, bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: Constant.sharedIns.HOMEPAGE_NAV)
        vc.modalPresentationStyle = .fullScreen
        self.present(vc, animated: true, completion: nil)
    }
    
    func createGradientLayer() {
        view.backgroundColor = UIColor.clear
        let backgroundLayer = colors.gl
        backgroundLayer?.frame = view.frame
        view.layer.insertSublayer(backgroundLayer!, at: 0)
    }
}

class gradientColors {
    var gl:CAGradientLayer!
    
    init() {
        let colorTop = UIColor(red: 255.0 / 255.0, green: 255.0 / 255.0, blue: 255.0 / 255.0, alpha: 1.0).cgColor
        let colorBottom = UIColor(red: 51.0 / 255.0, green: 176.0 / 255.0, blue: 74.0 / 255.0, alpha: 1.0).cgColor
        
        self.gl = CAGradientLayer()
        self.gl.colors = [colorTop, colorBottom]
        self.gl.locations = [0.15, 0.85]
    }
}

