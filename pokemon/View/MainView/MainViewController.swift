//
//  MainViewController.swift
//  pokemon
//
//  Created by Oscar Perdanakusuma Adipati on 03/12/20.
//

import Foundation
import UIKit
import Alamofire
import AlamofireObjectMapper
import SwiftyJSON
import Kingfisher
import CoreData

class MainViewController: BaseParentController {
    @IBOutlet weak var tableView: UITableView!
    var pokemonListModel : PokemonListModel?
    var pokemonModel : PokemonModel?
    var pokemonNames = [String]()
    var pokemonURLS = [String]()
    var pokemonImages = [String]()
    var pokemonIDS = [String]()
    var slots = [[Int]]()
    var slot = [Int]()
    var typeNames = [[String]]()
    var typeName = [String]()
    var corePokemonList: [NSManagedObject] = []

    override func viewDidLoad() {
        super.viewDidLoad()
        setEnableTapGestureOnMainView(isEnable: false)
        self.title = "Pokemon"
        tableView.delegate = self
        tableView.dataSource = self
        if(isConnectingInternet()==false){
            AlertViewUtil.sharedIns.showAlertFeedback(self, messageText: Wordings.msg_internet_error, actionButton1: Wordings.BTN_CLOSE, actionButton2: "")
            return
        } else {
            compareDataCount()
        }
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
        
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
    }
    
    func compareDataCount() {
        let url = NSURL(string:Constant.getAPIPokemonLimit())
        print("POKEMONS URL:\(String(describing: url!))")

        Alamofire.request(Constant.getAPIPokemonLimit(), method : .get, parameters: nil).responseJSON { response in
            switch(response.result) {
            case .success(_):
                if response.result.value != nil {
                    let json = JSON(response.result.value as Any)
                    CustomUserDefaults.shared.setAPICount("\(json.count)")
                    let apiCount = "\(CustomUserDefaults.shared.getAPICount())"
                    let coredataCount = "\(CustomUserDefaults.shared.getCoreDataCount())"
                    if (apiCount != coredataCount) {
                        self.callAPIPokemonList()
                    } else {
                        self.getCorePokemons()
                    }
                }
            case .failure(_):
                AlertViewUtil.sharedIns.showAlertFeedback(self, messageText: "Failed get Pokemon data", actionButton1: Wordings.BTN_CLOSE, actionButton2: "")
            }
        }
    }
    
    func callAPIPokemonList() {
        removeAll()
        let url = NSURL(string:Constant.getAPIPokemonLimit())
        print("POKEMON LIMIT URL:\(String(describing: url!))")
        showProgressDialog(message: Wordings.msg_please_wait)
        Alamofire.request(Constant.getAPIPokemonLimit(), method: .get, parameters: nil, encoding: JSONEncoding.default, headers: ConnectionManager.sharedIns.getHTTPHeadersSAMDB())
            .responseObject{ (response: DataResponse<PokemonListModel>) in
                self.dismissProgressDialog()
                switch(response.result) {
                case .success(_):
                    if response.result.value != nil {
                        self.pokemonListModel = response.result.value
                        let statusCode = response.response?.statusCode
                        print("statusCode:\(String(describing: statusCode))")
                        if (statusCode == 200){
                            let datas = self.pokemonListModel?.results
                            if ((datas?.count)! > 0) {
                                for i in 0..<(datas?.count)!  {
                                    let name = datas![i].name
                                    let url = datas![i].url
                                    let images = "https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/\(i + 1).png"
                                    let idn = self.getPokemonID(url: url!)
                                    self.pokemonNames.append(name!)
                                    self.pokemonURLS.append(url!)
                                    self.pokemonImages.append(images)
                                    self.pokemonIDS.append(idn)
                                    self.callAPIPokemon(name: name!)
                                }
                            } else {
                                self.setNoResult()
                            }
                        } else {
                            AlertViewUtil.sharedIns.showAlertFeedback(self, messageText: "Failed get Pokemon data", actionButton1: Wordings.BTN_CLOSE, actionButton2: "")
                        }
                    }
                case .failure(_):
                    AlertViewUtil.sharedIns.showAlertFeedback(self, messageText: "Failed get Pokemon data", actionButton1: Wordings.BTN_CLOSE, actionButton2: "")
                }
        }
    }
    
    func callAPIPokemon(name: String){
        let url = NSURL(string:Constant.getAPIPokemon()+"\(name)")
        print("POKEMON TYPE URL:\(String(describing: url!))")
        Alamofire.request(Constant.getAPIPokemon()+"\(name)", method: .get, parameters: nil, encoding: JSONEncoding.default, headers: ConnectionManager.sharedIns.getHTTPHeadersSAMDB())
            .responseObject{ (response: DataResponse<PokemonModel>) in
                switch(response.result) {
                case .success(_):
                    if response.result.value != nil {
                        self.pokemonModel = response.result.value
                        let statusCode = response.response?.statusCode
                        print("statusCode:\(String(describing: statusCode))")
                        if (statusCode == 200){
                            let datas = self.pokemonModel?.types
                            if ((datas?.count)! > 0) {
                                self.slot.removeAll()
                                self.typeName.removeAll()
                                for i in 0..<(datas?.count)!  {
                                    let slot = datas![i].slot
                                    let type = datas![i].typea?.name
                                    self.slot.append(slot!)
                                    self.typeName.append(type!)
                                }
                                self.slots.append(self.slot)
                                self.typeNames.append(self.typeName)
                                print("typeNames:\(self.typeNames)")
                                if (self.slots.count == Constant.getLimit() && self.typeNames.count == Constant.getLimit()) {
                                    self.corePokemons()
                                    DispatchQueue.main.async {
                                        self.lblOtherResult.isHidden = true
                                        self.showMainLayout(view: self.tableView)
                                        self.tableView.reloadData()
                                    }
                                }
                            } else {
                                self.slots.append([])
                                self.typeNames.append([])
                            }
                        } else {
                            AlertViewUtil.sharedIns.showAlertFeedback(self, messageText: "Failed get Pokemon data", actionButton1: Wordings.BTN_CLOSE, actionButton2: "")
                        }
                    }
                case .failure(_):
                    AlertViewUtil.sharedIns.showAlertFeedback(self, messageText: "Failed get Pokemon data", actionButton1: Wordings.BTN_CLOSE, actionButton2: "")
                }
        }
    }
    
    func setNoResult(){
        lblOtherResult.isHidden = false
        hideMainLayout(view: self.tableView)
        setLabelOtherResult(label: lblOtherResult, text: "No data", parentView: self.view)
    }
    
    func removeAll() {
        pokemonNames.removeAll()
        pokemonURLS.removeAll()
        pokemonImages.removeAll()
        pokemonIDS.removeAll()
        slots.removeAll()
        typeNames.removeAll()
    }
    
    func getPokemonID(url: String) -> String {
        let urlArr = url.components(separatedBy: "/pokemon/")
        let pokemonID = urlArr[1].replacingOccurrences(of: "/", with: "")
        return pokemonID
    }
    
    func corePokemons() {
        guard let appDelegate = UIApplication.shared.delegate as? AppDelegate else {
            return
        }
        let managedContext = appDelegate.persistentContainer.viewContext
        let fetchRequestPokemons = NSFetchRequest<NSManagedObject>(entityName: "Pokemon")
        do {
            let pokemons = try managedContext.fetch(fetchRequestPokemons)
            if (pokemons.count == 0) {
                for i in 0..<(pokemonIDS.count)  {
                    let insertPokemons = NSEntityDescription.insertNewObject(forEntityName: "Pokemon", into: managedContext)
                    insertPokemons.setValue(pokemonIDS[i], forKey: "id")
                    insertPokemons.setValue(pokemonNames[i], forKey: "name")
                    insertPokemons.setValue(pokemonImages[i], forKey: "image")
                    insertPokemons.setValue(pokemonURLS[i], forKey: "url")
                    insertPokemons.setValue(typeNames[i], forKey: "type")
                    do {
                        try managedContext.save()
                        corePokemonList.append(insertPokemons)
                    } catch let error as NSError {
                        print("Could not save. \(error), \(error.userInfo)")
                    }
                }
                CustomUserDefaults.shared.setCoreDataCount("\(corePokemonList.count)")
            } else {
                let deleteRequestPokemon = NSBatchDeleteRequest( fetchRequest: fetchRequestPokemons as! NSFetchRequest<NSFetchRequestResult>)
                do {
                    corePokemonList.removeAll()
                    try managedContext.execute(deleteRequestPokemon)
                    try managedContext.save()
                    for i in 0..<(pokemonIDS.count)  {
                        let insertPokemons = NSEntityDescription.insertNewObject(forEntityName: "Pokemon", into: managedContext)
                        insertPokemons.setValue(pokemonIDS[i], forKey: "id")
                        insertPokemons.setValue(pokemonNames[i], forKey: "name")
                        insertPokemons.setValue(pokemonImages[i], forKey: "image")
                        insertPokemons.setValue(pokemonURLS[i], forKey: "url")
                        insertPokemons.setValue(typeNames[i], forKey: "type")
                        do {
                            try managedContext.save()
                            corePokemonList.append(insertPokemons)
                        } catch let error as NSError {
                            print("Could not save. \(error), \(error.userInfo)")
                        }
                    }
                    CustomUserDefaults.shared.setCoreDataCount("\(corePokemonList.count)")
                } catch let error as NSError {
                    print("Could not delete. \(error), \(error.userInfo)")
                }
            }
        } catch let error as NSError {
            print("Could not fetch. \(error), \(error.userInfo)")
        }
    }
    
    func getCorePokemons() {
        removeAll()
        guard let appDelegate = UIApplication.shared.delegate as? AppDelegate else {
            return
        }
        let managedContext = appDelegate.persistentContainer.viewContext
        let fetchRequestPokemons = NSFetchRequest<NSManagedObject>(entityName: "Pokemon")
        do {
            corePokemonList = try managedContext.fetch(fetchRequestPokemons)
            CustomUserDefaults.shared.setCoreDataCount("\(corePokemonList.count)")
            if (corePokemonList.count > 0) {
                for pokemon in corePokemonList {
                    pokemonIDS.append(pokemon.value(forKey: "id") as? String ?? "")
                    pokemonNames.append(pokemon.value(forKey: "name") as? String ?? "")
                    pokemonImages.append(pokemon.value(forKey: "image") as? String ?? "")
                    pokemonURLS.append(pokemon.value(forKey: "url") as? String ?? "")
                    typeNames.append(pokemon.value(forKey: "type") as? [String] ?? [""])
                }
                DispatchQueue.main.async {
                    self.tableView.reloadData()
                }
            } else {
                callAPIPokemonList()
            }
        } catch let error as NSError {
            print("Could not fetch. \(error), \(error.userInfo)")
        }
    }
}

extension MainViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return pokemonNames.count
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return 40.0
    }

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 40.0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "mainCell", for: indexPath) as! mainTableViewCell
        let row = indexPath.row
        cell.lblPokemonName.text = pokemonNames[row]
        cell.lblPokemonName.textColor = .black
        let url = URL(string: pokemonImages[row])
        cell.imgPokemon?.kf.setImage(with: url)
        cell.imgPokemon.contentMode = .scaleToFill
        return cell
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        guard let tableViewCell = cell as? mainTableViewCell else { return }
        tableViewCell.setCollectionViewDataSourceDelegate(dataSourceDelegate: self, forRow: indexPath.row)
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath){
        let index = indexPath.row
        let storyboard = UIStoryboard(name: Constant.sharedIns.MAIN_STORYBOARD, bundle: nil)
        let navCon = storyboard.instantiateViewController(withIdentifier: Constant.sharedIns.DETAIL_NAV) as! UINavigationController
        let vc = navCon.viewControllers.first as! DetailViewController
        vc.tabBarController?.tabBar.isHidden = true
        vc.pokemonID = Int(pokemonIDS[index])
        vc.pokemonName = pokemonNames[index]
        vc.pokemonImage = pokemonImages[index]
        vc.pokemonTypesName = typeNames[index]
        navigationController?.pushViewController(vc, animated: true)
    }
}

extension MainViewController: UICollectionViewDelegate, UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView,
        numberOfItemsInSection section: Int) -> Int {
        return typeNames[collectionView.tag].count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "typesCell", for: indexPath) as! PokemonTypeCellView
        cell.imgPokemonType.image = UIImage(named: "\(typeNames[collectionView.tag][indexPath.item]).png")
        return cell
    }
    
    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAtIndexPath indexPath: NSIndexPath) -> CGSize {
        return CGSize(width: collectionView.frame.size.width, height: collectionView.frame.size.height)
    }
}
