//
//  DetailViewController.swift
//  pokemon
//
//  Created by Oscar Perdanakusuma Adipati on 04/12/20.
//

import Foundation
import UIKit
import Alamofire
import AlamofireObjectMapper
import SwiftyJSON
import Kingfisher
import CoreData
import ScrollingContentViewController

class DetailViewController: ScrollingContentViewController {
    @IBOutlet weak var imgPokemon: UIImageView!
    @IBOutlet weak var detailView: UIView!
    @IBOutlet weak var descriptionView: UIView!
    @IBOutlet weak var statsView: UIView!
    @IBOutlet weak var evolutionsView: UIView!
    @IBOutlet weak var lblPokemonName: UILabel!
    @IBOutlet weak var txtDescription: UITextView!
    @IBOutlet weak var txtAbilities: UITextView!
    @IBOutlet weak var imgPokemonNormal: UIImageView!
    @IBOutlet weak var imgPokemonShiny: UIImageView!
    @IBOutlet private weak var collectionPokemonTypeView: UICollectionView!
    @IBOutlet private weak var collectionWeaknessTypeView: UICollectionView!
    @IBOutlet weak var segmentedControl: UISegmentedControl!
    @IBOutlet private weak var statsTableView: UITableView!
    @IBOutlet private weak var captureTableView: UITableView!
    @IBOutlet private weak var breedingTableView: UITableView!
    @IBOutlet private weak var evolutionTableView: UITableView!
    var pokemonID: Int?
    var pokemonTypesName = [String]()
    var pokemonImage: String?
    var pokemonName: String?
    var captureRate: Int?
    var habitat: String?
    var generation: String?
    var hatch_counter: Int?
    var SpritesNormal: String?
    var SpritesShiny: String?
    var baseStats = [Int]()
    var statNames = [String]()
    var abNames = [String]()
    var weaknessNames = [String]()
    var weaknessDouble = [Double]()
    var eggGroupNames = [String]()
    var evolutionNames = [String]()
    var evolutionLevels = [Int]()
    var evolutionURLs = [String]()
    var female: Int?
    var pokemonSpeciesModel : PokemonSpeciesModel?
    var pokemonModel : PokemonModel?
    var pokemonTypeModel : PokemonTypeModel?
    var pokemonEvolutionModel : PokemonEvolutionModel?
    var corePokemonList: [NSManagedObject] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = pokemonName!
        collectionPokemonTypeView.delegate = self
        collectionPokemonTypeView.dataSource = self
        statsTableView.delegate = self
        statsTableView.dataSource = self
        collectionWeaknessTypeView.delegate = self
        collectionWeaknessTypeView.dataSource = self
        captureTableView.delegate = self
        captureTableView.dataSource = self
        breedingTableView.delegate = self
        breedingTableView.dataSource = self
        evolutionTableView.delegate = self
        evolutionTableView.dataSource = self
        if(isConnectingInternet()==false){
            AlertViewUtil.sharedIns.showAlertFeedback(self, messageText: Wordings.msg_internet_error, actionButton1: Wordings.BTN_CLOSE, actionButton2: "")
            return
        } else {
            callAPIPokemon()
        }
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setRoundedView(view: detailView)
        let url = URL(string: pokemonImage!)
        imgPokemon?.kf.setImage(with: url)
        imgPokemon.contentMode = .scaleToFill
        lblPokemonName.text = pokemonName!
        if segmentedControl.selectedSegmentIndex == 0 {
            evolutionsView.isHidden = true
            statsView.isHidden = false
        } else {
            statsView.isHidden = true
            evolutionsView.isHidden = false
        }
    }
        
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
    }
    
    func callAPIPokemon() {
        guard let appDelegate = UIApplication.shared.delegate as? AppDelegate else {
            return
        }
        let managedContext = appDelegate.persistentContainer.viewContext
        let fetchRequestDetail = NSFetchRequest<NSManagedObject>(entityName: "Detail")
        let idKeyPredicate = NSPredicate(format: "id == %@", "\(pokemonID!)")
        fetchRequestDetail.predicate = idKeyPredicate
        do {
            let details = try managedContext.fetch(fetchRequestDetail)
            if (details.count == 0) {
                callAPIPokemonSpecies(id: pokemonID!)
            } else {
                //get CoreData
                for pokemon in details {
                    captureRate = Int(exactly: pokemon.value(forKey: "captureRate") as! Int64)!
                    habitat = pokemon.value(forKey: "habitat") as? String ?? ""
                    generation = pokemon.value(forKey: "generation") as? String ?? ""
                    txtDescription.text = pokemon.value(forKey: "descriptions") as? String ?? ""
                    if (txtDescription.text == "No Data") {
                        txtDescription.textAlignment = .center
                    }
                    hatch_counter = Int(exactly: pokemon.value(forKey: "hatchCounter") as! Int64)!
                    SpritesNormal = pokemon.value(forKey: "spritesNormal") as? String ?? ""
                    SpritesShiny = pokemon.value(forKey: "spritesShiny") as? String ?? ""
                    female = Int(exactly: pokemon.value(forKey: "female") as! Int64)!
                    baseStats = pokemon.value(forKey: "baseStats") as? [Int] ?? [0]
                    statNames = pokemon.value(forKey: "statNames") as? [String] ?? [""]
                    abNames = pokemon.value(forKey: "abNames") as? [String] ?? [""]
                    weaknessNames = pokemon.value(forKey: "weaknessNames") as? [String] ?? [""]
                    weaknessDouble = pokemon.value(forKey: "weaknessDouble") as? [Double] ?? [0.0]
                    eggGroupNames = pokemon.value(forKey: "eggGroupNames") as? [String] ?? [""]
                    evolutionNames = pokemon.value(forKey: "evolutionNames") as? [String] ?? [""]
                    evolutionLevels = pokemon.value(forKey: "evolutionLevels") as? [Int] ?? [0]
                    evolutionURLs = pokemon.value(forKey: "evolutionURLs") as? [String] ?? [""]
                    txtAbilities.text = abNames.joined(separator: "\n")
                }
                reloadDatas()
                evolutionTableView.reloadData()
            }
        } catch let error as NSError {
            print("Could not fetch. \(error), \(error.userInfo)")
        }
    }
    
    func insertCoreData() {
        guard let appDelegate = UIApplication.shared.delegate as? AppDelegate else {
            return
        }
        let managedContext = appDelegate.persistentContainer.viewContext
        let insertPokemons = NSEntityDescription.insertNewObject(forEntityName: "Detail", into: managedContext)
        insertPokemons.setValue("\(pokemonID!)", forKey: "id")
        insertPokemons.setValue(Int64(captureRate!), forKey: "captureRate")
        insertPokemons.setValue(habitat, forKey: "habitat")
        insertPokemons.setValue(generation, forKey: "generation")
        insertPokemons.setValue(txtDescription.text, forKey: "descriptions")
        insertPokemons.setValue(hatch_counter, forKey: "hatchCounter")
        insertPokemons.setValue(SpritesNormal, forKey: "spritesNormal")
        insertPokemons.setValue(SpritesShiny, forKey: "spritesShiny")
        insertPokemons.setValue(baseStats, forKey: "baseStats")
        insertPokemons.setValue(statNames, forKey: "statNames")
        insertPokemons.setValue(abNames, forKey: "abNames")
        insertPokemons.setValue(weaknessNames, forKey: "weaknessNames")
        insertPokemons.setValue(weaknessDouble, forKey: "weaknessDouble")
        insertPokemons.setValue(eggGroupNames, forKey: "eggGroupNames")
        insertPokemons.setValue(evolutionNames, forKey: "evolutionNames")
        insertPokemons.setValue(evolutionLevels, forKey: "evolutionLevels")
        insertPokemons.setValue(evolutionURLs, forKey: "evolutionURLs")
        insertPokemons.setValue(Int64(female!), forKey: "female")
        do {
            try managedContext.save()
            corePokemonList.append(insertPokemons)
        } catch let error as NSError {
            print("Could not save. \(error), \(error.userInfo)")
        }
    }
    
    func callAPIPokemonSpecies(id: Int) {
        let url = NSURL(string:Constant.getAPIPokemonSpecies()+"\(id)")
        print("POKEMON SPECIES URL:\(String(describing: url!))")
        Alamofire.request(Constant.getAPIPokemonSpecies()+"\(id)", method: .get, parameters: nil, encoding: JSONEncoding.default, headers: ConnectionManager.sharedIns.getHTTPHeadersSAMDB())
            .responseObject{ (response: DataResponse<PokemonSpeciesModel>) in
                switch(response.result) {
                case .success(_):
                    if response.result.value != nil {
                        self.pokemonSpeciesModel = response.result.value
                        let statusCode = response.response?.statusCode
                        print("statusCode:\(String(describing: statusCode))")
                        if (statusCode == 200){
                            let evolution_chain = self.pokemonSpeciesModel?.evolution_chain?.url
                            self.captureRate = self.pokemonSpeciesModel?.capture_rate
                            self.habitat = self.pokemonSpeciesModel?.habitat?.name
                            self.generation = self.pokemonSpeciesModel?.generation?.name
                            let formDescriptionsDatas = self.pokemonSpeciesModel?.form_descriptions
                            let eggGroupDatas = self.pokemonSpeciesModel?.egg_groups
                            self.hatch_counter = self.pokemonSpeciesModel?.hatch_counter
                            self.female = self.pokemonSpeciesModel?.gender_rate
                            self.evolutionChain(url: evolution_chain!)
                            if ((eggGroupDatas?.count)! > 0) {
                                for i in 0..<(eggGroupDatas?.count)!  {
                                    let name = eggGroupDatas![i].name
                                    self.eggGroupNames.append(name!)
                                }
                            } else {
                            }
                            if ((formDescriptionsDatas?.count)! > 0) {
                                for i in 0..<(formDescriptionsDatas?.count)!  {
                                    let descriptiona = formDescriptionsDatas![i].descriptiona
                                    self.txtDescription.text = descriptiona
                                }
                            } else {
                                self.txtDescription.text = "No Data"
                                self.txtDescription.textAlignment = .center
                            }
                            self.callAPIPokemonStats(id: self.pokemonID!)
                        } else {
                            AlertViewUtil.sharedIns.showAlertFeedback(self, messageText: "Failed get Pokemon data", actionButton1: Wordings.BTN_CLOSE, actionButton2: "")
                        }
                    }
                case .failure(_):
                    AlertViewUtil.sharedIns.showAlertFeedback(self, messageText: "Failed get Pokemon data", actionButton1: Wordings.BTN_CLOSE, actionButton2: "")
                }
        }
    }
    
    func callAPIPokemonStats(id: Int) {
        baseStats.removeAll()
        statNames.removeAll()
        let url = NSURL(string:Constant.getAPIPokemon()+"\(id)")
        print("POKEMON STATS URL:\(String(describing: url!))")
        Alamofire.request(Constant.getAPIPokemon()+"\(id)", method: .get, parameters: nil, encoding: JSONEncoding.default, headers: ConnectionManager.sharedIns.getHTTPHeadersSAMDB())
            .responseObject{ (response: DataResponse<PokemonModel>) in
                switch(response.result) {
                case .success(_):
                    if response.result.value != nil {
                        self.pokemonModel = response.result.value
                        let statusCode = response.response?.statusCode
                        print("statusCode:\(String(describing: statusCode))")
                        if (statusCode == 200){
                            let Datas = self.pokemonModel?.stats
                            let Abilities = self.pokemonModel?.abilities
                            self.SpritesNormal = self.pokemonModel?.sprites?.front_default
                            self.SpritesShiny = self.pokemonModel?.sprites?.front_shiny
                            if ((Datas?.count)! > 0) {
                                for i in 0..<(Datas?.count)!  {
                                    let baseStat = Datas![i].base_stat
                                    let statName = Datas![i].stat?.name
                                    self.baseStats.append(baseStat!)
                                    self.statNames.append(statName!)
                                }
                            } else {
                            }
                            if ((Abilities?.count)! > 0) {
                                for i in 0..<(Abilities?.count)!  {
                                    let abName = Abilities![i].ability?.name
                                    self.abNames.append(abName!)
                                }
                                DispatchQueue.main.async {
                                    self.txtAbilities.text = self.abNames.joined(separator: "\n")
                                }
                            } else {
                                self.txtAbilities.text = "-"
                            }
                            self.callAPIWeakness(id: self.pokemonID!)
                        } else {
                            AlertViewUtil.sharedIns.showAlertFeedback(self, messageText: "Failed get Pokemon data", actionButton1: Wordings.BTN_CLOSE, actionButton2: "")
                        }
                    }
                case .failure(_):
                    AlertViewUtil.sharedIns.showAlertFeedback(self, messageText: "Failed get Pokemon data", actionButton1: Wordings.BTN_CLOSE, actionButton2: "")
                }
        }
    }
    
    func callAPIWeakness(id: Int) {
        weaknessNames.removeAll()
        weaknessDouble.removeAll()
        let url = NSURL(string:Constant.getAPIType()+"\(id)")
        print("POKEMON WEAKNESS TYPE URL:\(String(describing: url!))")
        Alamofire.request(Constant.getAPIType()+"\(id)", method: .get, parameters: nil, encoding: JSONEncoding.default, headers: ConnectionManager.sharedIns.getHTTPHeadersSAMDB())
            .responseObject{ (response: DataResponse<PokemonTypeModel>) in
                switch(response.result) {
                case .success(_):
                    if response.result.value != nil {
                        self.pokemonTypeModel = response.result.value
                        let statusCode = response.response?.statusCode
                        print("statusCode:\(String(describing: statusCode))")
                        if (statusCode == 200){
                            let DoubleDamages = self.pokemonTypeModel?.damage_relations?.double_damage_from
                            let HalfDamages = self.pokemonTypeModel?.damage_relations?.half_damage_from
                            if ((DoubleDamages?.count)! > 0) {
                                for i in 0..<(DoubleDamages?.count)!  {
                                    let dbDmg = DoubleDamages![i].name
                                    self.weaknessNames.append(dbDmg!)
                                    self.weaknessDouble.append(2.0)
                                }
                            } else {
                            }
                            if ((HalfDamages?.count)! > 0) {
                                for i in 0..<(HalfDamages?.count)!  {
                                    let hlDmg = HalfDamages![i].name
                                    self.weaknessNames.append(hlDmg!)
                                    self.weaknessDouble.append(0.5)
                                }
                            } else {
                            }
                            self.reloadDatas()
                            self.insertCoreData()
                        } else {
                            AlertViewUtil.sharedIns.showAlertFeedback(self, messageText: "Failed get Pokemon data", actionButton1: Wordings.BTN_CLOSE, actionButton2: "")
                        }
                    }
                case .failure(_):
                    AlertViewUtil.sharedIns.showAlertFeedback(self, messageText: "Failed get Pokemon data", actionButton1: Wordings.BTN_CLOSE, actionButton2: "")
                }
        }
    }
    
    func evolutionChain(url: String) {
        evolutionNames.removeAll()
        evolutionLevels.removeAll()
        Alamofire.request(url, method: .get, parameters: nil, encoding: JSONEncoding.default, headers: ConnectionManager.sharedIns.getHTTPHeadersSAMDB())
            .responseObject{ (response: DataResponse<PokemonEvolutionModel>) in
                switch(response.result) {
                case .success(_):
                    if response.result.value != nil {
                        self.pokemonEvolutionModel = response.result.value
                        let statusCode = response.response?.statusCode
                        print("statusCode:\(String(describing: statusCode))")
                        if (statusCode == 200){
                            let chain = self.pokemonEvolutionModel?.chain
                            let species1 = chain?.species?.name
                            let url1 = chain?.species?.url
                            self.evolutionNames.append(species1 ?? "")
                            self.evolutionLevels.append(1)
                            self.evolutionURLs.append(url1 ?? "")
                            let evolve = chain?.evolves_to
                            if ((evolve?.count)! > 0) {
                                for i in 0..<(evolve?.count)!  {
                                    let url2 = evolve![i].species?.url
                                    let species2 = evolve![i].species?.name
                                    let minLevel2 = evolve![i].evolution_details?[0].min_level
                                    self.evolutionNames.append(species2 ?? "")
                                    self.evolutionLevels.append(minLevel2 ?? 0)
                                    self.evolutionURLs.append(url2 ?? "")
                                    let evolve1 = evolve![i].evolves_to
                                    for i in 0..<(evolve1?.count)!  {
                                        let url3 = evolve1![i].species?.url
                                        let species3 = evolve1![i].species?.name
                                        let minLevel3 = evolve1![i].evolution_details?[0].min_level
                                        self.evolutionNames.append(species3 ?? "")
                                        self.evolutionLevels.append(minLevel3 ?? 0)
                                        self.evolutionURLs.append(url3 ?? "")
                                    }
                                }
                                DispatchQueue.main.async {
                                    self.evolutionTableView.reloadData()
                                }
                            } else {
                            }
                        } else {
                            AlertViewUtil.sharedIns.showAlertFeedback(self, messageText: "Failed get Pokemon data", actionButton1: Wordings.BTN_CLOSE, actionButton2: "")
                        }
                    }
                case .failure(_):
                    AlertViewUtil.sharedIns.showAlertFeedback(self, messageText: "Failed get Pokemon data", actionButton1: Wordings.BTN_CLOSE, actionButton2: "")
                }
        }
    }
    
    func reloadDatas() {
        DispatchQueue.main.async {
            self.captureTableView.reloadData()
            self.breedingTableView.reloadData()
            self.statsTableView.reloadData()
            self.collectionPokemonTypeView.reloadData()
            self.collectionWeaknessTypeView.reloadData()
            let urlNormal = URL(string: self.SpritesNormal!)
            self.imgPokemonNormal.kf.setImage(with: urlNormal)
            let urlShiny = URL(string: self.SpritesShiny!)
            self.imgPokemonShiny.kf.setImage(with: urlShiny)
        }
    }
    
    @IBAction func indexChanged(_ sender: UISegmentedControl) {
        switch sender.selectedSegmentIndex {
        case 0:
            evolutionsView.isHidden = true
            statsView.isHidden = false
        case 1:
            statsView.isHidden = true
            evolutionsView.isHidden = false
        default:
            break
        }
    }
    
    func replacedURL(row: Int) -> String {
        let urlArr = evolutionURLs[row].components(separatedBy: "/pokemon-species/")
        let pokemonID = urlArr[1].replacingOccurrences(of: "/", with: ".png")
        let replaceURL = "https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/" + pokemonID
        return replaceURL
    }
}

extension DetailViewController: UICollectionViewDelegate, UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView,
        numberOfItemsInSection section: Int) -> Int {
        if (collectionView == collectionPokemonTypeView) {
            return pokemonTypesName.count
        } else if (collectionView == collectionWeaknessTypeView) {
            return weaknessNames.count
        }
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if (collectionView == collectionPokemonTypeView) {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "pokemonTypeCell", for: indexPath) as! PokemonDetailTypeCellView
            cell.imgPokemonType.image = UIImage(named: "\(pokemonTypesName[indexPath.item]).png")
            return cell
        } else if (collectionView == collectionWeaknessTypeView) {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "weaknessTypeCell", for: indexPath) as! PokemonWeaknessTypeCellView
            cell.imgWeaknessType.image = UIImage(named: "\(weaknessNames[indexPath.item]).png")
            cell.lblWeakStats.text = "\(weaknessDouble[indexPath.item])x"
            cell.lblWeakStats.textColor = .black
            return cell
        }
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "pokemonTypeCell", for: indexPath) as! PokemonDetailTypeCellView
        cell.imgPokemonType.image = UIImage(named: "\(pokemonTypesName[indexPath.item]).png")
        return cell
    }
    
    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAtIndexPath indexPath: NSIndexPath) -> CGSize {
        return CGSize(width: collectionView.frame.size.width, height: collectionView.frame.size.height)
    }
}

extension DetailViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if (tableView == statsTableView) {
            return baseStats.count
        } else if (tableView == captureTableView) {
            return 1
        } else if (tableView == breedingTableView) {
            return eggGroupNames.count
        } else if (tableView == evolutionTableView) {
            return evolutionNames.count
        }
        return 1
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        if (tableView == statsTableView || tableView == captureTableView || tableView == breedingTableView) {
            return 20.0
        } else if (tableView == evolutionTableView) {
            return 120.0
        }
        return 120.0
    }

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if (tableView == statsTableView || tableView == captureTableView || tableView == breedingTableView) {
            return 20.0
        } else if (tableView == evolutionTableView) {
            return 120.0
        }
        return 120.0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if (tableView == statsTableView) {
            let cell = tableView.dequeueReusableCell(withIdentifier: "statsCell", for: indexPath) as! statsTableViewCell
            let row = indexPath.row
            cell.lblStatsName.text = statNames[row]
            cell.lblStatsInt.text = "\(baseStats[row])"
            cell.progStats.progress = Float(baseStats[row])/Float(100)
            return cell
        } else if (tableView == captureTableView) {
            let cell = tableView.dequeueReusableCell(withIdentifier: "captureCell", for: indexPath) as! captureTableViewCell
            cell.lblHabitat.text = self.habitat
            cell.lblGeneration.text = self.generation
            cell.lblCaptureRate.text = String(format: "%.2f", (Double(self.captureRate ?? 0) / Double(255))*100)+"%"
            return cell
        } else if (tableView == breedingTableView) {
            let cell = tableView.dequeueReusableCell(withIdentifier: "breedingCell", for: indexPath) as! breedingTableViewCell
            let row = indexPath.row
            cell.lblEggGroup.text = eggGroupNames[row]
            cell.lblHatchTime.text = "\(255 * (hatch_counter! + 1)) steps"
            cell.lblGender.text = "\((Double(self.female!))/(Double(8))*100)% female"
            return cell
        } else if (tableView == evolutionTableView) {
            let cell = tableView.dequeueReusableCell(withIdentifier: "evolutionCell", for: indexPath) as! evolutionTableViewCell
            let row = indexPath.row
    
            if (row == 0) {
                cell.lblName1.text = pokemonName
                cell.lblName1.textColor = .black
                let url = URL(string: pokemonImage!)
                cell.imgSpecies1?.kf.setImage(with: url)
                cell.imgSpecies1.contentMode = .scaleToFill
                cell.lblName2.text = evolutionNames[row]
                cell.lblName2.textColor = .black
                let url1 = URL(string: replacedURL(row: row))
                cell.imgSpecies2?.kf.setImage(with: url1)
                cell.imgSpecies2.contentMode = .scaleToFill
                cell.lblLevel.text = "Lv \(evolutionLevels[row])"
                cell.lblLevel.textColor = .black
            } else if (row == 1) {
                cell.lblName1.text = evolutionNames[row - 1]
                cell.lblName1.textColor = .black
                let url = URL(string: replacedURL(row: row - 1))
                cell.imgSpecies1?.kf.setImage(with: url)
                cell.imgSpecies1.contentMode = .scaleToFill
                cell.lblName2.text = evolutionNames[row]
                cell.lblName2.textColor = .black
                let url1 = URL(string: replacedURL(row: row))
                cell.imgSpecies2?.kf.setImage(with: url1)
                cell.imgSpecies2.contentMode = .scaleToFill
                cell.lblLevel.text = "Lv \(evolutionLevels[row])"
                cell.lblLevel.textColor = .black
            } else if (row == 2) {
                cell.lblName1.text = evolutionNames[row - 1]
                cell.lblName1.textColor = .black
                let url = URL(string: replacedURL(row: row - 1))
                cell.imgSpecies1?.kf.setImage(with: url)
                cell.imgSpecies1.contentMode = .scaleToFill
                cell.lblName2.text = evolutionNames[row]
                cell.lblName2.textColor = .black
                let url1 = URL(string: replacedURL(row: row))
                cell.imgSpecies2?.kf.setImage(with: url1)
                cell.imgSpecies2.contentMode = .scaleToFill
                cell.lblLevel.text = "Lv \(evolutionLevels[row])"
                cell.lblLevel.textColor = .black
            }
            return cell
        }
        let cell = tableView.dequeueReusableCell(withIdentifier: "statsCell", for: indexPath) as! statsTableViewCell
        let row = indexPath.row
        cell.lblStatsName.text = statNames[row]
        cell.lblStatsInt.text = "\(baseStats[row])"
        cell.progStats.progress = Float(baseStats[row])/Float(100)
        return cell
    }
}
