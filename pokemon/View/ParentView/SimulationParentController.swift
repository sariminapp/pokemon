//
//  SimulationParentController.swift
//  homecredit
//
//  Created by Oscar Perdanakusuma - Private on 06/12/17.
//  Copyright © 2017 Home Credit Indonesia. All rights reserved.
//

import UIKit

class SimulationParentController : MainParentController{
    
    var amountValue : Double = 0
    var amountText : String = ""
    var maxAmount : Double = 0
    var minAmount : Double = 0
    var isInputEmpty = false
    var isMinimumAmountTriggered = false
    
    func textFieldDidChanged(_ sender: UITextField){
        let length = (sender.text?.count)!
        amountText = sender.text!
        print("CHANGE: \(amountText)")
        
        if length > 0{
            isInputEmpty = false
            
            setAmountValue()
            
            if maxAmount > 0 && (getAmountValue() > maxAmount){
                amountValue = maxAmount
            }
            
            setAmountWithNumberFormat()
            sender.text = getAmountWithNumberFormat()
        }else{
            amountValue = 0
            isInputEmpty = true
        }
        
        if amountValue >= minAmount{
            isMinimumAmountTriggered = false
        }else{
            isMinimumAmountTriggered = true
        }
        
    }
    
    func setAmountValue(){
        amountValue = Double(amountText.replacingOccurrences(of: ".", with: ""))!
    }
    
    func getAmountValue()->Double{
        return amountValue
    }
    
    func setAmountWithNumberFormat(){
        amountText = CommonUtils.shared.getCurrencyLocaleFormatWithoutSymbol(amount: amountValue)
    }
    
    func getAmountWithNumberFormat() -> String{
        return amountText
    }
    
}
