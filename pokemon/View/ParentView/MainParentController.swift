//
//  MainParentView.swift
//  homecredit
//
//  Created by Oscar Perdanakusuma on 8/15/17.
//  Copyright © 2017 Home Credit Indonesia. All rights reserved.
//

import UIKit

class MainParentController: BaseParentController{
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        setNavBarThemeColor()
    }
}


