//
//  FeedbackDialogUtil.swift
//  homecredit
//
//  Created by Oscar Perdanakusuma - Private on 9/11/17.
//  Copyright © 2017 Home Credit Indonesia. All rights reserved.
//

import UIKit

class FeedbackDialog: UIView{
    
    var imageDone = UIImageView(frame: CGRect.zero)
    var roundedDialogView = UIView(frame: CGRect.zero)
    let messageLabel = UILabel(frame: CGRect.zero)
    
    
    enum type{
        case success
        case failed
    }
    

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    
    init(message: String, type: type) {
        super.init(frame: CGRect.zero)
        
        backgroundColor = UIColor(white: 0.0, alpha: 0.3)
        
        //print("MESSAGE_LOADING:\(message)")
        //set label first
        messageLabel.text = message
        messageLabel.textColor = UIColor.white
        //messageLabel.font = UIFont(name: "HelveticaNeue-Light",size: 12.0)
        messageLabel.font = messageLabel.font.withSize(16)
        messageLabel.textAlignment = .center
        messageLabel.numberOfLines = 0
        
        
        
        var icDone : UIImage?
        
        switch type{
        case .success:
             icDone = UIImage(named: "ic_done_black")
            break
        case .failed:
            icDone = UIImage(named: "ic_close_big")
            break
        }
        
        icDone = icDone?.tintImage(color: UIColor.white)
        imageDone = UIImageView(image: icDone)
        
        //let visualEffect = UIBlurEffect(style: UIBlurEffectStyle.dark)
        //roundedDialogView = UIVisualEffectView(effect: visualEffect)
        roundedDialogView.backgroundColor = UIColor(string: Constant.sharedIns.color_black)
        roundedDialogView.alpha = 0.85
        
        roundedDialogView.layer.cornerRadius = 14.0
        roundedDialogView.clipsToBounds = true
        
        roundedDialogView.frame.size.width = 240.0
        roundedDialogView.frame.size.height = 200.0
        roundedDialogView.frame.origin.x = ceil((bounds.width / 2.0) - (roundedDialogView.frame.width / 2.0))
        roundedDialogView.frame.origin.y = ceil((bounds.height / 2.0) - (roundedDialogView.frame.height / 2.0))
        
        imageDone.frame.origin.x = ceil((bounds.width / 2.0) - (imageDone.frame.width / 2.0))
        imageDone.frame.origin.y = ceil((bounds.height / 1.3) - (imageDone.frame.height / 1.3))
        
        
        let messageLabelSize = messageLabel.sizeThatFits(CGSize(width:200.0 - 15.0 * 2.0, height: CGFloat.greatestFiniteMagnitude))
        messageLabel.frame.size.width = messageLabelSize.width
        messageLabel.frame.size.height = messageLabelSize.height
        messageLabel.frame.origin.x = ceil((bounds.width / 2.0) - (messageLabel.frame.width / 2.0))
        messageLabel.frame.origin.y = ceil(imageDone.frame.origin.y + imageDone.frame.size.height + ((roundedDialogView.frame.height - imageDone.frame.height) / 4.0) - (messageLabel.frame.height / 2.0))
        
        addSubview(roundedDialogView)
        addSubview(imageDone)
        addSubview(messageLabel)
        
    }
    
}
